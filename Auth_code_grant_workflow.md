# Steps for Authorization code grant type

## Client registration
- client id - `0oa2xj0jm8kVA5hME5d7`
- client secret - `JBaWeGF8frTz6xOkYrFfXeg54xP3piNutpbpK_4c`
- Redirect URI - `http://localhost:8555/oauth/callback`

- Meta data url - `https://dev-7858070.okta.com/oauth2/default/.well-known/oauth-authorization-server`

- issuer - `https://dev-7858070.okta.com/oauth2/default`

- authorization endpoint -  `https://dev-7858070.okta.com/oauth2/default/v1/authorize`
- token endpoint - `https://dev-7858070.okta.com/oauth2/default/v1/token`
- jwks uri -  `https://dev-7858070.okta.com/oauth2/default/v1/keys`

- Link - `https://developer.okta.com/docs/guides/implement-grant-type/authcode/main/#flow-specifics`

## Auth code grant flow

 1. User will be redirected to Auth server endpoint

`
 https://dev-7858070.okta.com/oauth2/default/v1/authorize?client_id=0oa2xj0jm8kVA5hME5d7&response_type=code&scope=openid&redirect_uri=http%3A%2F%2Flocalhost%3A8555%2Foauth%2Fcallback&state=state-a7f62dae-519b-11ec-bf63-0242ac130002
`
### Front channel
- Response from Auth server - `http://localhost:8555/oauth/callback?code=UWtcpHYBQsUt7YHK_eWep0NdOiFEHpTKen1Ho-hwDEI&state=state-a7f62dae-519b-11ec-bf63-0242ac130002
`
### Back channel
- Authorization code - `UWtcpHYBQsUt7YHK_eWep0NdOiFEHpTKen1Ho-hwDEI`
### Exchange authorization code for ID token and Access token 
`
curl --location --request POST 'https://dev-7858070.okta.com/oauth2/default/v1/token' \
--header 'accept: application/json' \
--header 'authorization: Basic MG9hMndtem9oM05nRVh0U1A1ZDc6SkRocmRjX0ZyUkpXQ3hrYVVsUG5uUEswZ0R1TWZZM0VxaUlVSnhjNg==' \
--header 'content-type: application/x-www-form-urlencoded' \
--header 'Cookie: JSESSIONID=A0A8F8EE06BC997F6B5BF2E9D8B84EBC' \
--data-urlencode 'grant_type=authorization_code' \
--data-urlencode 'redirect_uri=http://localhost/8555/oauth/callback' \
--data-urlencode 'code=UWtcpHYBQsUt7YHK_eWep0NdOiFEHpTKen1Ho-hwDEI'
`

package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import static java.util.stream.IntStream.range;

@Configuration
@RequiredArgsConstructor
public class BootstrapConfig implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;

    private Faker faker = new Faker();
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        range(1, 100).forEach((index) -> {
            Order order= Order.builder()
                        .orderPrice(faker.number().randomDouble(2, 20000, 30000))
                        .orderDate(faker.date().past(6, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                        .lineItems(new HashSet<>())
                        .customerName(faker.name().fullName())
                        .emailAddress(faker.internet().emailAddress())
                        .build();

            range(1,4).forEach(value -> {
                LineItem lineItem = LineItem.builder()
                                            .qty(faker.number().numberBetween(2,4))
                                            .price(faker.number().randomDouble(2, 10000, 25000))
                                            .name(faker.commerce().productName())
                                            .build();
                order.addLineItem(lineItem);
            });
            this.orderRepository.save(order);
        });
    }
}
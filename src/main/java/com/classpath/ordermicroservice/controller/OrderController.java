package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrder(){
        log.info("Fetching all the orders");
        return this.orderService.fetchOrders();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable long id){
        log.info("Fetching order by id");
        return orderService.fetchOrderByOrderId(id);
    }

    @PostMapping
    public Order saveOrder(@RequestBody Order order){
        log.info("Saving the order , {}", order);
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable long id){
        this.orderService.deleteOrderById(id);
    }
}
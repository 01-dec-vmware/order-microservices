package com.classpath.ordermicroservice.event;

public  enum EventType {
    ORDER_PENDING,
    ORDER_ACCEPTED,
    ORDER_REJECTED,
    ORDER_CANCELLED,
    ORDER_FULFILLED
}

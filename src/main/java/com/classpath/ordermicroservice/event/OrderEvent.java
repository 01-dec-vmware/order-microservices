package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public final class OrderEvent {

    private  EventType eventType;

    private  LocalDateTime timestamp;

    private  Order order;
}
package com.classpath.ordermicroservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="line_items")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = "order")
@EqualsAndHashCode(exclude = "order")
public class LineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int itemId;

    private String name;

    private double price;

    private int qty;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;
}
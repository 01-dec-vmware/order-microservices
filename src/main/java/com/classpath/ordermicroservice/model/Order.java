package com.classpath.ordermicroservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    private String customerName;

    private LocalDate orderDate;

    private double orderPrice;

    private String emailAddress;

    @OneToMany(mappedBy = "order", cascade = ALL, fetch = EAGER)
    private Set<LineItem> lineItems;

    //scaffolding method to set the links on both the sides
    public void addLineItem(LineItem lineItem){
        if (this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }

}
package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.EventType;
import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static com.classpath.ordermicroservice.event.EventType.ORDER_ACCEPTED;
import static java.time.LocalDateTime.now;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final KafkaTemplate<Long, OrderEvent> kafkaTemplate;

    private final RestTemplate restTemplate;
    /* No need of the constructor since we have added the @RequiredArgsConstructor
    public OrderService(OrderRepository orderRepository){
        this.orderRepository = orderRepository;
    }*/
    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallBack")
    @Retry(name="retryService", fallbackMethod = "fallBack")
    public Order saveOrder(Order order){
        log.info("Inside the saveorder method :: {]", order);
        //call the persistence layer and assign the order id
        Order savedOrder = this.orderRepository.save(order);
        log.info("Processing the order accepted Event");
       // final ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://inventory-service/api/v1/inventory", null, Integer.class);
        //log.info("Response from Inventory microservice :: {} ", responseEntity.getBody());

        //push the order event to the message broker
        OrderEvent orderEvent = OrderEvent.builder().eventType(ORDER_ACCEPTED).timestamp(now()).order(savedOrder).build();

//        ProducerRecord<Long, Order> record = new ProducerRecord<Long, Order>("orders-topic", Long.class, Order.class);
        //publish this  event to the message broker
        kafkaTemplate.send("orders-topic", savedOrder.getOrderId(), orderEvent);
        return  savedOrder;
    }

    private Order fallBack(Exception exception){
        log.error("Exception while connecting to inventory microservice , {}" , exception.getMessage());
        return Order.builder().emailAddress("contact@comp.com").orderDate(LocalDate.now()).build();
    }

    public Set<Order> fetchOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order fetchOrderByOrderId(long orderId){
        return this.orderRepository
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid order id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}
package com.classpath.ordermicroservice.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderTests {

    @Test
    public void testOrderConstructor(){
        Order order = new Order();
        assertNotNull(order);
    }

    @Test
    public void testSetters(){
        Order order = Order
                            .builder()
                            .orderId(12)
                            .orderDate(LocalDate.now())
                            .customerName("vinod")
                            .orderPrice(25000)
                        .build();

        assertNotNull(order);

        assertEquals(12, order.getOrderId());
        assertEquals("vinod", order.getCustomerName());
    }
}